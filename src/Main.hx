import hxd.Window;
import hxd.Key;
import GameMap.Coord2d;
import hxd.res.Resource;

typedef Level =  {
    map_data : Resource,
    playerInitPos : Coord2d,
    boxesInitPos : Array<Coord2d>
}

class Main extends hxd.App {

    var levels : Array<Level>;
    var currentLevel : Int;
 
    var player : Player;
    
    var push_pos : Coord2d;
    static var sensibility : Int = 300;

    static function main() {
        hxd.Res.initEmbed();
        new Main();
    }

    override private function init() {
        super.init();

        levels = [
            {
                map_data: hxd.Res.tilemap_data,
                playerInitPos: {x:10,y:8},
                boxesInitPos : [{x: 11, y: 8}, {x: 12, y: 8}]
            },
            {
                map_data: hxd.Res.map2_data,
                playerInitPos: {x:3,y:3},
                boxesInitPos : [{x:1 , y: 2}, {x: 2, y: 2}]
            }
        ];

        currentLevel = 0;
        new GameMap(s2d, levels[currentLevel].map_data);
        player = new Player(s2d, levels[currentLevel].playerInitPos);
        new BoxManager(s2d, levels[currentLevel].boxesInitPos.copy());
        Window.getInstance().addEventTarget(onEvent);
    }

    private function onEvent(event : hxd.Event) {
        if (event.kind == EKeyUp)
        {
            switch (event.keyCode) {
                case Key.DOWN:
                    player.move(DOWN);
                case Key.UP:
                    player.move(UP);
                case Key.LEFT:
                    player.move(LEFT);
                case Key.RIGHT: 
                    player.move(RIGHT);
                case Key.R:
                    setLevel();
            }
        }
        else if (event.kind == EPush)
        {
            push_pos = { x: Std.int(event.relX), y: Std.int(event.relY) };
        }
        else if (event.kind == ERelease)
        {
            var rel_pos = { x: Std.int(event.relX), y: Std.int(event.relY) };
            var diffx = rel_pos.x - push_pos.x;
            var diffy = rel_pos.y - push_pos.y;
            if (Math.abs(diffx) >= sensibility || Math.abs(diffy) >= sensibility)
            {
                if (Math.abs(diffx) > Math.abs(diffy))
                    player.move(diffx > 0 ? RIGHT : LEFT);
                else
                    player.move(diffy > 0 ? DOWN : UP);
            }
        }
    }

    private function setLevel(level : Int = -1) {
        if (level != -1)
        {
            currentLevel = level;
            GameMap.getInstance().setMap(levels[currentLevel].map_data);
        }
        player.setPosition(levels[currentLevel].playerInitPos);
        BoxManager.getInstance().setBoxes(s2d, levels[currentLevel].boxesInitPos.copy());
    }

    override private function update(dt) {
        if (GameMap.getInstance().checkVictory())
        {
            if (currentLevel < levels.length)
                setLevel(currentLevel+1);
        }
    }
}