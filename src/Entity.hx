import h2d.Tile;
import h2d.Anim;
import h2d.Scene;
import GameMap.Coord2d;

enum Directions {
    UP;
    DOWN;
    LEFT;
    RIGHT;
}

class Entity {
    var animation : Anim;
    var position : Coord2d;

    var tile : Tile;

    public function new(scene : Scene, initialPos : Coord2d) {
        position = { x: initialPos.x, y: initialPos.y };
        
        var tilesize =  GameMap.getInstance().getTileSize();
        animation = new h2d.Anim([ for (i in 0 ... Std.int(tile.width / tilesize.x)) tile.sub(i*tilesize.x, 0, tilesize.x, tilesize.y)], 5, scene);
        update();
    }

    public function setPosition(pos : Coord2d) {
        position = { x: pos.x, y: pos.y };
        update();
    }

    public function getPosition() : Coord2d {
        return position;
    }

    public function move(direction : Directions, canMoveBox : Bool = true) : Bool {
        var moved : Bool = false;
        var box : Box;
        var canMove : Bool = true;
        var map = GameMap.getInstance();

        switch (direction) {
            case UP:
                box = BoxManager.getInstance().getBox({x : position.x, y : position.y - 1});
            case DOWN:
                box = BoxManager.getInstance().getBox({x : position.x, y : position.y + 1});
            case LEFT:
                box = BoxManager.getInstance().getBox({x : position.x - 1, y : position.y});
            case RIGHT:
                box = BoxManager.getInstance().getBox({x : position.x + 1, y : position.y});
        }

        if (box != null) {
            if (!canMoveBox)
                return false;
            
            canMove = box.move(direction, false);
        }
        
        if (!canMove)
            return false;

        switch (direction) {
            case UP:
                if (map.getTileType(position.x, position.y - 1) != WALL) {
                    position.y --;
                    moved = true;
                }
            case DOWN:
                if (map.getTileType(position.x, position.y + 1) != WALL) {
                    position.y ++;
                    moved = true;
                }
            case LEFT:
                if (map.getTileType(position.x - 1, position.y) != WALL) {
                    position.x --;
                    moved = true;
                }
            case RIGHT:
                if (map.getTileType(position.x + 1, position.y) != WALL) {
                    position.x ++;
                    moved = true;
                }
        }
        update();
        return moved;
    }

    private function update() {
        var tileSize = GameMap.getInstance().getTileSize();
        animation.x = position.x * tileSize.x;
        animation.y = position.y * tileSize.y;
    }
}