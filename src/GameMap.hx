import hxd.res.Resource;
import h2d.TileGroup;
import h2d.Tile;
import h2d.Scene;

enum TileType {
    WALL;
    GROUND;
    TARGET;
}

typedef TilemapData = { layers:Array<{ data:Array<Int>}>, tilewidth:Int, tileheight:Int, width:Int, height:Int };
typedef Coord2d = {x : Int, y : Int};

class GameMap {
    private static var instance : GameMap;
    private var scene : Scene;

    private var group : TileGroup;
    private var tilemap_image : Tile;
    
    private var tiles_type : Array<TileType>;
    // width and height of the tiles
    private var tile_width : Int;
    private var tile_height : Int;
    // number of lines and columns of the map
    private var map_columns : Int;
    private var map_lines : Int;

    public static function getInstance() : GameMap {
        return instance;
    }

    public function getTileType(x : Int, y : Int) : TileType {
        if (x >= 0 && x < map_columns && y >= 0 && y < map_lines)
            return tiles_type[x + y * map_columns];
        return WALL;
    }

    public function getSize() : Coord2d {
        return {
            x : map_columns * tile_width,
            y : map_lines * tile_height
        };
    }

    public function getTileSize() : Coord2d {
        return {
            x : tile_width,
            y : tile_height
        };
    }

    private function id_to_type(id : Int, tw : Int) : TileType
    {
        if (id == 0) return WALL;
        id --;
        var x : Int = id % tw;
        var y : Int = Std.int((id-x) / tw);
        if ((x >= 1 && x <= 4) && (y >= 1 && y <= 3))
            return GROUND;
        else if (x == 6 && y < 2)
            return TARGET;
        else
            return WALL;
    }

    public function new(scene : Scene, map_data : Resource) {
        instance = this;
        this.scene = scene;
        
        
        tilemap_image = hxd.Res.tilemap_image.toTile();
        group = new h2d.TileGroup(tilemap_image, scene);
        setMap(map_data);
    }

    public function checkVictory() : Bool {
        for (y in 0 ... map_lines) for (x in 0 ... map_columns) {
            var bm = BoxManager.getInstance();
            if (tiles_type[x + y * map_columns] == TARGET && bm.getBox({x: x, y: y}) == null)
                return false;
        }
        return true;
    }

    public function setMap(map_data : Resource) {
        var tilemap_data : TilemapData = haxe.Json.parse(map_data.entry.getText());

        group.clear();

        tile_width = tilemap_data.tilewidth;
        tile_height = tilemap_data.tileheight;
        map_columns = tilemap_data.width;
        map_lines = tilemap_data.height;

        var tilemap_width = Std.int(tilemap_image.width / tile_width);
        var tilemap_height = Std.int(tilemap_image.height / tile_height);
        
        var tiles = [
            for(y in 0 ... tilemap_height)
            for(x in 0 ... tilemap_width)
            tilemap_image.sub(x * tile_width, y * tile_height, tile_width, tile_height)
        ];

        // we only take the data from one layer
        tiles_type = [ for (y in 0 ... map_lines) for (x in 0 ... map_columns)
            id_to_type(tilemap_data.layers[0].data[x + y * map_columns], tilemap_width) ];
        
        for (y in 0 ... map_lines) for (x in 0 ... tilemap_data.width)
        {
            var tile_id = tilemap_data.layers[0].data[(x + y * map_columns)];
            if (tile_id > 0)
                group.add(x * tile_width, y * tile_height, tiles[tile_id - 1]);
        }

        scene.camera.setViewport(0,0, map_columns * tile_width, map_lines * tile_height);
        scene.setScale(Math.min(scene.width / scene.camera.viewportWidth, scene.height / scene.camera.viewportHeight));
    }
}