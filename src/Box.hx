import h2d.Scene;
import GameMap.Coord2d;

class Box extends Entity {

    public var enabled(default, set) : Bool;

    public function new(scene : Scene, initialPos : Coord2d) {
        tile = hxd.Res.box.toTile();

        super(scene, initialPos);
    }

    public function set_enabled(enabled : Bool) : Bool{
        animation.visible = enabled;
        this.enabled = enabled;
        return enabled;
    }
}