import h2d.Scene;
import GameMap.Coord2d;

class BoxManager {
    var boxes : Array<Box>;

    static var instance : BoxManager;

    public static function getInstance() : BoxManager {
        return instance;
    }

    public function new(scene : Scene, coords : Array<Coord2d>) {
        instance = this;
        boxes = [];
        setBoxes(scene, coords);
    }

    public function getBox(coord : Coord2d) {
        for (box in boxes) {
            if (box.enabled) {
                if (box.getPosition().x == coord.x && box.getPosition().y == coord.y)
                    return box;
            }
            // if the box was disabled, all following ones will be
            else
                return null;
        }
        return null;
    }

    public function setBoxes(scene : Scene, coords : Array<Coord2d>) {
        var i = 0;
        if (boxes != null) {
            // we use the allocated boxes as mush as we can
            while (i < boxes.length && i < coords.length) {
                boxes[i].setPosition(coords[i]);
                boxes[i].set_enabled(true);
                i++;
            }
            // if coords.length < boxes.length, we disable all other boxes
            while (i < boxes.length) {
                boxes[i].set_enabled(false);
                i++;
            }
        }
        // if boxes.length < coords.length we need to allocate more boxes
        while (i < coords.length) {
            boxes.push(new Box(scene, coords[i]));
            boxes[i].set_enabled(true);
            i++;
        }
    }
}