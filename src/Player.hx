import h2d.Scene;
import GameMap.Coord2d;

class Player extends Entity {

    public function new(scene : Scene, initialPos : Coord2d) {
        tile = hxd.Res.player.toTile();
        super(scene, initialPos);
    }

}