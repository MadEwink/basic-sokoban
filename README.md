# Sokoban

This project is a small sokoban game made with [heaps](https://heaps.io/) in order to get used to it.
Learn more about [Haxe](https://haxe.org/) too.

## Play it now

You can try it [in your browser](https://madewink.gitlab.io/basic-sokoban).

### Controls

Keyboard : be sure to click on the game area, otherwise your inputs will be ignored. Use the keyboard arrows to move, and press R to reset the current level.

Tactile : swipe any of the four directions to move.

## Requirements

In order to compile this project you need `haxe` and `heaps` (see links above).

## Compile

Then you can simply compile from the directory root using

```haxe --js <path/to/filename.js> -L heaps -p src -m Main```

or

```haxe compile.hxml```

You'll also need a html file `index.html` such as the one given [here](https://heaps.io/documentation/hello-world.html).
Then you can open the html file in your browser.

## Credits

The graphic assets I used are from [there](https://pixel-poem.itch.io/dungeon-assetpuck).
The map was created using [Tiled](https://www.mapeditor.org/).
